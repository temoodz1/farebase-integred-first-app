package com.example.farebasehomework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registration.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        Signinbutton.setOnClickListener {

            if (EmailField.text.isEmpty() && Passfiled.text.isEmpty()) {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show()
            } else if (EmailField.text.isEmpty() && Passfiled.text.isNotEmpty()) {
                Toast.makeText(this, "Please enter your email", Toast.LENGTH_LONG).show()
            } else if (EmailField.text.isNotEmpty() && Passfiled.text.isEmpty()) {
                Toast.makeText(this, "Please enter your Password", Toast.LENGTH_LONG).show()
            } else {
                auth.signInWithEmailAndPassword(
                    EmailField.text.toString(),
                    Passfiled.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            val intent = Intent(this, Picpage::class.java)
                            startActivity(intent)
                        } else {
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }


                    }


            }

        }
        SignupButton.setOnClickListener {

            val intent = Intent(this, Registration::class.java)
            startActivity(intent)

        }

    }

}


