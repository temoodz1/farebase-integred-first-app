package com.example.farebasehomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registration.*

class Registration : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        Registrationbutton1.setOnClickListener {
            if (EmailField1.text.isEmpty() && Passfiled1.text.isEmpty()) {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show()
            } else if (EmailField1.text.isEmpty() && Passfiled1.text.isNotEmpty()) {
                Toast.makeText(this, "Please enter your email", Toast.LENGTH_LONG).show()
            } else if (EmailField1.text.isNotEmpty() && Passfiled1.text.isEmpty()) {
                Toast.makeText(this, "Please enter your Password", Toast.LENGTH_LONG).show()
            } else {
                auth.createUserWithEmailAndPassword(
                    EmailField1.text.toString(),
                    Passfiled1.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this,
                                "Registration was Successful. please return in to login page",
                                Toast.LENGTH_LONG
                            ).show()

                        } else {

                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()


                        }
                    }
            }
        }
    }
}
